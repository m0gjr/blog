---
title: Contact
layout: page
---

If you a reading this blog you probably already know who I am and can
reach out directly.

If this site has somehow taken off in popularity and you want to ask
questions/discuss content of this blog, you can reach out via email to:

```
m0gjr dot alex plus blog at googlemail dot com
```
If you are unable to construct a valid email address from those
instructions you are probably a robot and I do not want your spam!
