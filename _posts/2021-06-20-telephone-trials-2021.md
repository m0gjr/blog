At this time of the year, I typically head off to the internet
to survey what new products are available in the inexpensive
(mostly) Chinese manufactured smartphone market.

I normally buy a new phone at this time in an attempt to address
some of the issues i have with the previous one if i can find
a new model that meets my selection criteria.

This year, as you will see below, things got a little out
of hand!

![phones](/blog/assets/images/phones1.png)

# Sizing and grip

I consider a telephone to be a device to be (primarily)
operated with one hand. Unfortunately most modern smartphone
manfacturers disagree with this position and sell enormous
monstrosities requiring two handed operation some of which
are so large they don't effectively fit into a pocket!

As an aside, i've never been particularly enamoured with the
tablet computer concept. As a device that requires two hands
to operate and fits into a rucksack rather than a pocket,
they appear to me to be a lightweight underpowered laptop
with a much worse user interface.

In order to qualify for 'one handed operation' a telephone
needs to meet two criteria simultaniously:

1. can be held (comforably) in one hand without dropping
2. can reach the whole screen surface with the thumb
of the holding hand without adjusting grip

Most modern smartphone are too wide to be held within the
fingers and appear to be delibrately made out of the most
slippery materials yet discovered by mankind.
Purchasing aftermarket cases in an attempt to address these
deficiencies makes the phone even wider and often doesn't
add enough grip to achieve a secure hold.

Even if a phone can be held (or precariously balanced)
on the fingers of a hand to allow the full range of motion
for the thumb to operate, many phones have a large enough
screen size to make it uncomfortable/impossible to reach
the entire screen area from status bar to nagivation bar
in a single range of motion.

Apparently very few phone manufacturers seem manufacture
devices in (what i would consider) the 'optimum' size
category: as large as can be comfortably used with one
hand but no larger. Most target the large end (5-7 inch
screen size) with a smaller number targetting the
"worlds smallest Android phone" market with devices
that appear to be designed solely with the aim of being
smuggled rectally into prisons with no concern given to
being large enough to actually type on!

# Requirements

My ideal telephone would meet the following hardware criteria:

* appropriate size (approximately 4 inch screen)
* appropriate construction (rubber overmoulded plastic
to provide a secure grip)
* plastic screen (scratches pose a minor cosmetic 
annoyance compared to unusability of a shattered
glass screen)
* long battery life
* 3.5mm headphone jack
* senstiive LTE/GPS radios
* powerful enough processor to render webpages promptly

On the software side, there are many things that I would
*like* to have but are typically constrained by the limits
of the Android platform and any 'features' included in the
version provided with the phone.

# The incumbant

For the past year, I have been using a Cubot King Kong Mini
as my primary telephone. This was purcahsed in 2020 as it
was the only new production smartphone on the market of an
appropriate size. Also I have been generally satisfied with
the Cubot brand and it was under £100.

![cubot](/blog/assets/images/cubot1.png)

## Dimensions

* Length: 120mm
* Overall width: 60mm
* Grip width (with modifications): 55mm
* Thickness: 12.7mm
* Screen: 100mm (corner to corner)

After a few minutes with a screwdriver and hacksaw, I was able to
remove most of the aluminium rails that run down both sides of
the phone making it slightly narrower and leaving a grippy plastic
lip around the edge.

This puts the Cubot at pretty much the *perfect* size to
hold between middle fingers/palm while being able to reach
the whole screen surface with my thumb.

## Hardware

The Cubot King Kong is a ruggedised smartphone that is
_supposed_ to be waterproof to IP68 rating (submersible).
This waterproof rating is only with the rubber cover sealing
the charge port and since my phones get charged much more
often than they get submersed.... I removed the charge
port cover and permanently fitted a magnetic charged adaptor
(to prevent the eventual failure of the usb connector when
repeatedly plugging in/out a charge cable).

Unfortunately, shortly after unboxing the phone I first realised
that it did not come with a 3.5mm audio jack (presumably due to
waterproofing requirements). This led to me investigating many
different Bluetooth/USB headphone options none of which gave
entirely satisfactory results (to be addressed in a separate post).

## Software

This phone came with Android 8, later upgrading to Android 9.
This OS version ruined (in my option) the ability to mute
microphone input in the phone app nessesitating external mic
mute to be used on conference calls. Fortunately, world events
lead to the majority of my business related calls taking place
over Microsoft Teams or other computer based video conferencing
tools and so these difficulties were never fully realised.

Also new in Android 8 was changes to the automatic screen
brightness adjustment. The previous fixed calibration table
which could be set once and worked *adaquately* for most
lighting conditions was replaced with an 'adaptive learning'
mechanism which appeared (so far as i could tell) would
randomly set screen brightness to the wrong value while
ignoring any user input to train it (full brightness while
outside in direct sunlight and minimum brightness in the dark
of night *should* not be a difficult thing to work out!)

Since I was effectively forced to manually set the screen
brightness *every* time i opened the phone rather than just
when i moved into a new environment I opted to disable auto
brightness and manually set the brightness (with an app to
cut down on button presses) when going outside.

# The first challenger

The first shots were fired in this field trial when i discovered
the Satrend S11 4G smartphone available on Amazon for £70.
While slightly smaller than desired (3.2 inch screen), this
was within my preferred price bracket with all the functionality
I was looking for and well worth purchasing for further
investigation.

![satrend](/blog/assets/images/satrend1.png)

## Dimensions

All dimensions are inclusive of the included rubber case

* Length: 102mm
* Width: 53mm
* Thickness: 11.7mm
* Screen: 82mm

The Satrend fit comforatbly in the hand and pocket but the on
screen keyboard was slightly too small to accurately type the
correct keys all the time. This may have still given acceptable
service if spell cheek were to be enabled.

## Hardware

The phone seemed to have adaqute processing power and battery
life.

While the radio did support 4G/LTE connections, it appears
that there was some band support/sensitivity issues that
prevented a reliable connection. After several incidences
of not getting a mobile data connection while out in an urban
area, use of this device was terminated.

While the telephone performed adaquately with headphones,
the volume was excessive for handheld calls even at the
minimum volume level and had to be held away from the ear
to reduce the volume to a comfortable level.

## Software

Using Android 7.1 the phone provided optimal settings for
screen brightness control and phone settings.

This version of Android would eventually become unsuported by
required apps (WhatsApp is currently supported on Android 4.4
and Firefox Android 5.0).

# My first Samsung

Shortly after the purchase of the Satrend but before it arrived
I was already lining up a potential successor when i realised
refurbised Samsung Galaxy S4 mini devices were avaiable on Ebay
for £35 each. Naturally i bought one to see how an older name
brand phone would compare to brand new models from less
expensive suppliers.

![samsung](/blog/assets/images/samsung1.png)

## Dimensions

Dimensions of the bare phone:

* Length: 125mm
* Width: 61mm
* Thickness: 9.2mm
* Screen only: 110mm
* Screen inc nav buttons: ~120mm

The phone is slightly wider than ideal for my hand size and
as slippery as an eal in it's raw form. Adding an aftermarket
silcone case made the device even wider and wasn't grippy
enough now that it was slipping out of my hand due to the
width.

I found enabling the on screen navigation bar (disabling the
hardware buttons) decreased the effective travel size sufficiently
to just fit within my thumb's range of motion.

After a full disassmbly I was able to insert self amalgamating
tape into the seams of the phone and join on extra tape to
wrap around the back of the device.

This only adds a couple milimeters to the device width while
giving a soft sticky rubber coating without any unpleasent
sticky residue.

Other products may also be effective such as grip tape for
sporting equipment but I did not have any to hand for testing.

![grip tape](/blog/assets/images/samsung2.png)

Note my use of my telephone as a physical key management solution.

## Hardware

The phone is somewhat slow to render webpages (not helped due to
my usage of Firefox and installing add ons (ublock-origin and
dark reader), this is probably down to the older slower processor
compared to other phones in the trials.

This phone is equiped with an AMOLED screen (my first) and a large
battery giving exceptionally good battery life for a smartphone.

The power button barely extends past the edge of the case making
power on/off somewhat diffcult.

It appears the SD card reader fitted to the device is slow and
has performance troubles. Attempting to use the SD card to
supplement the internal 8GB of storage resulted in Apps failing
to start on boot and not running/configuring reliably. Apparently
all apps would need to be installed on the system volume and only
using the SD Card for external data/music etc.

## Software

When I purchased the phone I was aware the stock android (4.4) was
going to be pretty much useless from the get go. Luckly, being
a name brand phone it is extremely well supported for custom ROMs.

The phone is offically supported by TWRP recovery and doesn't have
any OEM restrictions on the bootloader allowing the recovery to
be flashed just by booting up with the home/vol down buttons
pressed, plugging into the computer with a USB cable and flashing
a new recovery image using Odin/Heimdall.

The device is no longer offically supported by LineageOS but a
keen developer has kept releasing unoffical build images online.
I installed LineageOS 18.1 (Android 11) and went ahead with setup.

Being LineageOS, it did not come with Google apps preinstalled
(this can be added separately if desired) so I did not need to
undergo my usual steps to disable/restrict all of Googles apps
before replacing with my own.

Also, Android has *finally* introduced some much needed changes
to the UI/functionality like using the volume controls to adjust
the media volume not notifications volume. This cut down on the
number of extra apps that needed to be installed to make a usable
system.

Unfortunately, the phone app does not support VoLTE/Wifi calling.
This appears to be a software/driver issue as the original hardware
did support VoLTE for some carrier provided ROMs. If this cannot
be addressed in the future this may impose a hard limit on the
phone's service life to the lifespan of the 2G/3G networks in the
UK.

# The rabbit hole

After purchasing the first Samsung S4 mini and successfully flashing
it I almost immedately bought another one so that I could run
comparison testing between two idential phones for different
configurations/situations.

I also bought an S5 mini (the last mini Samsung Galaxy) for 
comparison but decided it was slightly too large for use as a
primary phone. It did have a nice rubber overmoulded case that
served as inspiration for the self amalgamating tape construction.

# Conclusions

Currently I'm using my modified S4 mini as my primary telephone
and am relatively happy with it in this role. I'm maintaining
the Cubot as a standby ready to swap back if any issues arise
and will probably keep a lookout (next year) to see if anything
else comes on the market.

I may fully disassemble the S4 (the spare one) and take a file to
the case in order to slim a few milimeters off the width to get
a better grip.

Any further conclusions will be dealt with in a separate post as
this has dragged on *much* longer than I anticipated writing for!
