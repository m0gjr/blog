---
title: The Wonders of CI/CD
---

This blog is written using [Jekyll](https://jekyllrb.com/) and hosted
using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

These tools allow the creation of a static site by parsing
a collection of Markdown HTM and other configL files stored in my
[Git repository](https://gitlab.com/m0gjr/blog)
and rendering them into static HTML which can be inexpensively
hosted by GitLab for free.
This saves on the complexity and cost of running databases
in order to host read only static content which never updates
as a result of user interaction.

The modern 'trendy' way to get such content rendered is to use
the Continuous Integration/Continuous Development tools
[usually] provided in your Git repository.
This allows you to simply edit your content and push it to the
repository and have the site automatically rendered and hosted.

GitLab provides a [template](https://gitlab.com/pages/jekyll)
prebuilt to render such a blog (which, incidentally is why
you have any CSS to make this site look pretty) which can just
be forked and edited to suit your preferences/deploy content
(although [not working](https://gitlab.com/pages/jekyll/-/issues/40)
out of the box it turns out).

# How this works

The mechanism used in CI/CD pipelines is typically Docker
and a base image is spun up each time the pipeline is run
(every time you push to the repository).
Jekyll is a ruby package (gem) which needs to be installed
on the base ruby image each time this occurs

Installing all these packages (from the
[gemfile](https://gitlab.com/m0gjr/blog/-/blob/master/Gemfile))
takes approximately
[4 minutes](https://gitlab.com/m0gjr/blog/-/jobs)
of run time on the container and is required
every time the repository gets updated.

Given my estimates on the readership of this blog
this means the average compute time to generate is probably
in the region of 1 minute per page view!
This probably still compares highly favourably to the overhead of
idling a small database and server side scripting when unused though.

# Outcome

While much less efficient than rendering the site locally on my laptop
(which already has all the dependancies installed and and regenarate
the site in a fraction of a second every time i alter a file),
There isn't really a good reason to alter the workflow.

Swaping to a prerendered site would require changing the
[CI/CD config](https://gitlab.com/m0gjr/blog/-/blob/master/.gitlab-ci.yml)
which would require me to have a better understanding of
how this works and would also tie me in to only editing the blog
from somewhere with ruby/jekyll installed.

GitLab gives all users 400 minutes of CI/CD for free each month
and so unless i start editing this blog multiple times per
day every day i am unlikely to exceed this limit.

# Update

Part way through writing this (so not _really_ an update),
i noticed [another issue](https://gitlab.com/pages/jekyll/-/issues/38)
on the parent repository which addresses this exact point.
Apparently enabling caching in the repository so prerequisites
(if unchanged since last time) don't need to be installed every time
cuts the build time down to _only_ 1 minute.

Whether this would be worthwhile to enable in the future
would require consideration because caching **always** has the
[capability to cause unexpected problems](https://www.martinfowler.com/bliki/TwoHardThings.html)
if it doesn't invalidate properly.
