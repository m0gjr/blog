## Background

If you want to achieve a high degree of anonymity online, one of the
most recommended and effective ways to reach is is with the
[Tails operating system](https://tails.boum.org/) (note, merely visiting
the site *may* result in you being added to an NSA watchlist according to an
[investigation](https://daserste.ndr.de/panorama/aktuell/NSA-targets-the-privacy-conscious,nsa230.html)
by a German TV network).

This operating system combines the use of the
[Tor network](https://www.torproject.org/) to route network traffic
anonymously on the Internet along with a network configuration to force all
software on the machine to connect via Tor blocking unproxied internet access.

Additionally the OS comes configured as a 'live cd' operating system
designed to be booted from non-persistent media leaving no local trace
of what activities have been performed once the computer has powered down.
A large selection of useful software is preinstalled for various purposes
to expand the versatility of the system without needing to download
additional software.

## Storage

Obviously a computer without any storage can be a considerable limitation
for most purposes other than anonymous browsing of websites.
To work around this limitation, there are two main options:

* local storage
* cloud storage

The benefits between these options may depend on your system usage,
anticipated threat profile and legal jurisdiction in which you operate.

### Local storage

While Tails does not by default store any data persistently and *could*
be booted from a ~DVD~ _digital gramophone, it supports the capability to
add an encrypted persistent volume to the USB flash disk/memory card you
are _almost certainly_ using.

Persistent volumes are encrypted using LUKS/dm-crypt and allow storage of
personal data, settings, addtional software etc.

Using password based encryption with appropriate key stretching can result
in a large degree of protection against brute force attack provided
[appropriate entropy passwords](https://xkcd.com/936/) are used.

In certain uncivilised parts of the world (such as the United Kingdom)
law enforcement can legally require encryption keys to be handed over.

### Cloud storage

Many services such as email and cryptocurrency require server hosting in
any case and so could be used without local storage relying purely on
cloud storage of persistent data.
Cloud storage solutions may also be appropriate for other uses such as
configuration or file storage if a suitable provide can be found.

Carefull selection of external 3rd parties for storage of personal data is
of particular importance when attempting to reach a high level of anonymity.
Systems which fully encrypt user data with keys derived from a user password
not known by the provider would be highly recommended and consideration
should be given to the security/privacy record of the provider organisation
and legal jurisdiction in which they operate.

To prevent de-anonymisation of your Tor anonymised profile it is critical
to *never* access any accounts from outside Tor.
If paid online services are used, these may be tracable back to a real
life person by attackers who can obtain payment details.

In some cases use of online storage may offer additional plausable deniability
leaving no physical evidence on the machine that any particular service
is used by the user.

## Hardware

While just about any personal computer using amd64 processor architecture
could be used with Tails, a laptop is probably recommended for a self
contained mobile platform as well allowing better physical security to
protect an unattended device from installation of keyloggers, malware
and rootkits.

For a dedicated set up, unneeded storage/networking cards, audio interfaces
and webcams may be disabled in BIOS settings or physically removed to reduce
the attack surface in the event of the device being remotely compromised.

## Connectivity

Finally on to the intended scope of this post, is the networking
capabilities of the Tails system.
With no persistent storage, WiFi passwords cannot be saved on the device
and would need to be inputted every time the computer boots up.
Ethernet is not fitted to many modern laptops and may not be available
in the intended environment for usage.
Having a secondary device to store any necessary configuration/software for
the upstream internet connection would be a useful capability to reduce
the need to configure a system every boot.

I decided to try this with a Raspberry Pi Zero W to act as a WiFi adaptor
for a Tails laptop.
Since it would be straightforward to do so, I configured the Pi to also
run a VPN client and force all Internet traffic for the Tails computer
over the VPN interface effectively 'dual stacking' Tor over the VPN.

Running both a VPN and Tor gives additional protection against an attacker
who can compromise the Tor network, and running the VPN on a physically
separate device gives an additional protection against a compromise of the
main laptop or leakage of its 'raw' internet IP.
Running the VPN 'underneath' Tor on its upstream network connection allows
it to be provisioned without any alteration to the (complex and security
critical) Tails networking configuration and allows a paid VPN account to
be used without risk of deanonymising the Tor connection through the
payment provider.

## Config

Configuration of the Raspberry Pi was performed using a
[rc.local file](https://gitlab.com/m0gjr/bashcrap/-/blob/master/conf/rpi-vpn.local).
This can be added to Rasbian or (probably) any other Linux Raspberry Pi
operating system along with a couple of additional local files for configuring
the VPN connection and WiFi access points.

Once booted up the first time, the Raspberry Pi will configure itself and
install any needed software before rebooting.
Each subsequent time the Pi boots up it will connect to the Internet,
connect to the configured VPN and then enable routing from the Tails machine
and the VPN interface.
Automatic software update is configured every boot but this occurs after
network forwarding has commenced to shorten the network startup process.

In the tested system the Raspberry Pi was configured as a USB OTG
slave device so that a virtual Ethernet over USB connection would be
presented to the Tails laptop.
On Linux these connections automatically configure without any additional
software/drives present and a small DHCP server on the Raspberry Pi causes
the Tails device to automatically configure connectivity on boot.

The timescales required for boot up approximately match on the devices tested
so both the Pi commences routing at approximately the time the Tails computer
attempts to establish Tor and connect to the Internet.

## Futher considerations

In this example WiFi is the only supported uplink capability but other
alternatives such as Bluetooth/USB tethering to a telephone, Ethernet etc
could be achieved with different hardware.

I'm unsure what level of attack surface USB OTG exposes to allow the
Raspberry Pi and Tail laptop to attack each other.
If this attack is of particular concern, a full sized Raspberry Pi
connected to the Laptop over (real) ethernet may be a superious option.

Using a USB flash drive and a Pi, two USB sockets on the Laptop were needed.
With a 2 wire USB cable to prevent any data connection, charging a mobile
telephone for a complete mobile set up from a single battery/mains power
supply would be possible.

Captive portals are not readily accessible with this setup due to the
inability to access the 'upstream' WiFi connection from any web browser.
Even if the 'insecure' browser provided in Tails for situations like this
were to be enabled, additional configuration such as a SOCKS proxy would be
needed on the VPN Pi to bypass the VPN out.
These bypasses would require addional steps each boot up and would introduce
additional risk of bypassing the provided anonymity.

MAC address cloning from a separate device with a web broswer may be possible
with most captive portal systems, but in any case mobile data is likely to
be a superiour solution where a high degree of anonymity is desired.
