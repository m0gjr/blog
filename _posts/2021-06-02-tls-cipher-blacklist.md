---
title: TLS Cipher Suite Blacklisting
---

## The Begining

A few months ago while ~pratting around~
_conducting important security research_,
i started blacklisting all outdated TLS cipher suites
in my browser that don't use ephemeral key exchange to support
perfect forward secrecy. (i also blacklisted triple DES because
[apparently](https://3des.badssl.com/) that's still a thing somehow?)

Since then i've not noticed any issues with using the internet
or having any site connect properly.

Until...

## Drumroll

![](/blog/assets/images/tlscipher1.png)

Yep, looks like our company pension provider forgot to configure
the TLS settings on the 'secure' log in section of their site.

The Extended Validation certificate combined with deprecated
ciphers is a particularly nice touch.
