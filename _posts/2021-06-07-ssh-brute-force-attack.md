---
title: SSH Brute Force Attack
---

# Background

For reasons outside the scope of this post, my home server
does not have any default routing via my internet connection.
All IP routing is forced via the WireGuard tunnel to
[my VPN provider](https://mullvad.net)
(Mullvad were the only VPN provider who supported WireGuard
at the time i selected them).

One of the results of this configuration is that i cannot get
port forwarding set up on well known ports such as 22 or 443.
I can only get random high numbered ports forwarded to my machine.
Interestingly this doesn't significantly impede setting up
TLS using LetsEncrypt certificates so long as you can set
DNS TXT records.

# SSH

Since I was running my SSH server on a random high numbered
port i was rather surprised one day when process treeing
my server to see SSHD repeatedly spawning off child processes
for what turned out to be login attempts coming from the internet.
Reviewing the authentication logs showed I was getting 10000
failed login attempts per day trying to brute force the
password to my server.

Most of these attempts were to guess the password of the root
account (which by default is set to only allow key based
authentication in most linux distributions) with some attempts
against a varienty of plausable system user accounts
(rsync/administrator etc).

I didn't bother trying to analyse what IP addresses the
connections were originating from. They are most likely
very poorly secure IOT devices or other easily compromised
systems commonly used to make botnets.

# Password entropy

This is the reason why it is recommended to disable password
authentication when exposing SSH over the internet (or even
to not expose SSH at all). If you don't know how to properly
estimate password entropy the botnets *will* attempt to measure
it for you.

In my case, i like to retain the capability to remote into my
home server from external devices that do not have my SSH key
stored on them. I also like to be able to use
[shellinabox](https://github.com/shellinabox/shellinabox)
to connect via a web browser from a device that does not have
a native SSH client.

I do understand entropy and worked out my password would last
about 30000 years to brute force at the rate they were going.

I added an extra 10 characters just to be safe.

# Conclusions

Based on this experience it does raise questions about the
value of generating interesting and useful content to try
and become popular on the interent.

![ssh-auth](/blog/assets/images/ssh-auth1.png)

Why try to make content people want to read when you can just
hook up an SSH server and let the 'friend requests' come rolling
in by the thousands?!
